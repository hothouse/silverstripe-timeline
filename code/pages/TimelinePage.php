<?php

class TimelinePage extends Page {

}

class TimelinePage_Controller extends Page_Controller {

	private static $allowed_actions = array('getJSONData');

	public function TimeLine() {
		Requirements::javascript(MODULE_TIMELINE_PATH.'/thirdparty/timeline/js/storyjs-embed.js');
		return $this->renderWith('TimeLineInclude');
	}

	public function getJSONDates() {
		$DateObjects = PageDate::get()->filter('ShowInTimeline', true);
		$Dates = array();
		foreach($DateObjects as $DateObject) {
			// no year, no display
			if(!$DateObject->Year) {
				continue;
			}

			// text to display
			$Text = $DateObject->Content;

			// get related page
			if($DateObject->PageID != $this->ID) {
				$Page = $DateObject->Page();

				// override text
				if($Page && !$Text && $Page->ID) {
					$Text = isset($Page->IntroText) && $Page->IntroText ? $Page->IntroText : $Page->obj('Content')->FirstParagraph();
				}

				// add link
				if($Page && $DateObject->DisplayLinkToPage && $Page->isPublished()) {
					$Text .= '<br /><a href="'.$Page->Link().'">find out more</a>';
				}
			}

			// no text, no display
			if(!$Text) {
				continue;
			}

			// fix text to proper display
			$Text .= '&nbsp;';

			// get dates
			$Date = array();
			$Date[] = sprintf('%04d', $DateObject->Year);
			if($DateObject->Month) {
				$Date[] = sprintf('%02d', $DateObject->Month);
				if($DateObject->Day) {
					$Date[] = sprintf('%02d', $DateObject->Day);
				}
			}

			// prepare array for timeline
			$Array = array(
				'startDate' => implode(',', $Date),
				'endDate' => '',
				'headline' => $DateObject->Title,
				'text' => $Text
			);
			if($DateObject->Image()->ID && $RatioImage = $DateObject->Image()->SetRatioSize($this->config()->get('image_width'), $this->config()->get('image_height'))) {
				$Array['asset'] = array(
					'media' => $RatioImage->Url,
					'credit' => '',
					'caption' => ''//$DateObject->Image()->Title
				);
			} elseif($Video = $DateObject->Video) {
				if($VideoData = $Video->VideoData()) {
					$Array['asset'] = array(
						'media' => $VideoData->VideoUrl,
						'credit' => '',
						'caption' => $VideoData->Title
					);
				}
			}
			array_push($Dates, $Array);
		}

		return $Dates;
	}

	public function getJSONData() {
		$Data = array(
			'timeline' => array(
				'headline' => $this->Title,
				'type' => 'default',
				'text' => $this->Content,
				'date' => $this->getJSONDates()
			)
		);
		return Convert::array2json($Data);
	}

}
