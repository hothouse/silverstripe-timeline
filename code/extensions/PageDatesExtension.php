<?php

class PageDatesExtension extends DataExtension {

	private static $has_many = array(
		'Dates' => 'PageDate'
	);

	public function updateCMSFields(FieldList $fields) {
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(50),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm()
		);
		$gridField = new GridField("Dates", "Dates", $this->owner->Dates(), $gridFieldConfig);
		$fields->addFieldToTab("Root.Dates", $gridField);
	}

}