<?php

class PageDate extends DataObject {

	private static $db = array(
		'Title' => 'Varchar(255)',
		'Content' => 'HTMLText',
		'Day' => 'Int',
		'Month' => 'Int',
		'Year' => 'Int',
		'Video' => 'Video',
		'ShowInTimeline' => 'Boolean',
		'DisplayLinkToPage' => 'Boolean'
	);

	private static $summary_fields = array(
		'Title' => 'Title',
		'Date' => 'Date',
		'ShowInTimeline' => 'ShowInTimeline'
	);

	private static $has_one = array(
		'Page' => 'Page',
		'Image' => 'Image'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('PageID');

		// checkbox
		if(!$this->ID || $this->ClassName == 'TimelinePage') {
			$fields->removeByName('DisplayLinkToPage');
		} else {
			$fields->addFieldToTab("Root.Main", new CheckboxField('DisplayLinkToPage', 'Display link to this page'));
		}

		// dates
		$Days = array();
		for ($i=0; $i < 32; $i++) {
			$Days[] = $i;
		}
		$Months = array();
		for ($i=0; $i < 13; $i++) {
			$Months[] = $i;
		}
		$fields->replaceField('Day', DropDownField::create('Day', 'Day', $Days));
		$fields->replaceField('Month', DropDownField::create('Month', 'Month', $Months));

		return $fields;
	}

	/**
	 * Getter for Date
	 * @return string
	 */
	public function forTemplate() {
		return $this->getDate();
	}
	public function getDate() {
		return DBField::create_field('Date', sprintf('%02d/%02d/%04d', $this->Day, $this->Month, $this->Year));
	}

	/**
	 * Setter for Date
	 * @param $value
	 */
	public function setDate($value) {
		/* @var $Date SS_Datetime */
		$Date = DBField::create_field('Date', $value);
		$this->record['Day'] = (int)$Date->DayOfMonth();
		$this->record['Month'] = (int)$Date->format('n');
		$this->record['Year'] = (int)$Date->Year();
	}
}
